const responseData = [
  {
    name: "dominos",
    rating: 4.5,
    ETA: "30 mins",
    menu: ["pizza", "tasty pizza", "shitty pizza"],
  },
  {
    name: "MCD",
    rating: 4.9,
    ETA: "30 mins",
    menu: ["burger", "tasty pizza", "shitty pizza"],
  },
  {
    name: "dhaba",
    rating: 4.5,
    ETA: "3 mins",
    menu: ["pizza", "tasty pizza", "shitty pizza"],
  },
  {
    name: "samose wala",
    rating: 5,
    ETA: "20 mins",
    menu: ["pizza", "tasty pizza", "shitty pizza"],
  },
  {
    name: "noodles",
    rating: 4.5,
    ETA: "30 mins",
    menu: ["pizza", "tasty pizza", "shitty pizza"],
  },
  {
    name: "chinese food",
    rating: 3.5,
    ETA: "24 mins",
    menu: ["noodles1", " pizza", "noodle pizza"],
  },
];

const createMenuList = (menuList) => {
  const menuContent = document.createElement("div");
  menuContent.setAttribute("class", "menuList");
  menuList.forEach((menuItem) => {
    const item = document.createElement("button");
    item.setAttribute("class", "menuItem");
    item.textContent = menuItem;
    menuContent.appendChild(item);
  });
  return menuContent;
};
const createHotelCard = (hotel) => {
  const newItem = document.createElement("div");
  newItem.setAttribute("class", "card");

  const topSection = document.createElement("div");
  const middleSection = document.createElement("div");
  const bottomSection = document.createElement("div");

  const img = document.createElement("img");
  img.setAttribute("src", "https://source.unsplash.com/random/100x100");
  topSection.appendChild(img);

  const hotelName = document.createElement("span");
  const rating = document.createElement("span");
  const ETA = document.createElement("span");
  hotelName.textContent = hotel.name;
  rating.textContent = hotel.rating;
  ETA.textContent = hotel.ETA;
  middleSection.appendChild(hotelName);
  middleSection.appendChild(rating);
  middleSection.appendChild(ETA);

  const menuHeader = document.createElement("span");
  menuHeader.textContent = "Menu";
  const menuList = createMenuList(hotel.menu);
  bottomSection.appendChild(menuHeader);
  bottomSection.appendChild(menuList);

  topSection.setAttribute("class", "topSection");
  middleSection.setAttribute("class", "middleSection");
  bottomSection.setAttribute("class", "bottomSection");

  newItem.appendChild(topSection);
  newItem.appendChild(middleSection);
  newItem.appendChild(bottomSection);

  return newItem;
};

const createInitialRender = (root, hotelsList) => {
  hotelsList.forEach((hotel) => {
    const hotelCard = createHotelCard(hotel);
    root.appendChild(hotelCard);
  });
};

//render our content based on the data we get
const resturantsList = document.querySelector(".content");
createInitialRender(resturantsList, responseData);

const addItemInOrderedList = (orderedItem) => {
  const newItem = document.createElement("div");
  newItem.setAttribute("class", "orderItem");

  const hotel = orderedItem.parentNode.parentNode.parentNode;
  const hotelNameTemp = hotel.querySelector(".middleSection").firstChild;
  const hotelName = hotelNameTemp.cloneNode(true);

  
  const foodName = orderedItem.innerHTML;
  const orderedFood = document.createElement('span');
  orderedFood.setAttribute('class','orderedFoodName');
  orderedFood.textContent = foodName;

  const deleteButton = document.createElement("button");
  deleteButton.setAttribute("class", "orderedFoodDelete");
  deleteButton.textContent = "Cancel";

  newItem.appendChild(hotelName);
  newItem.appendChild(orderedFood);
  newItem.appendChild(deleteButton);
  return newItem;
};

document.querySelector(".content").addEventListener("click", (e) => {
  const orderedItem = e.target;

  const orderedList = document.querySelector("#orderList");
  orderedList.appendChild(addItemInOrderedList(orderedItem));
});

document.querySelector("#orderList").addEventListener("click", (e) => {
  let clickedNode = e.target;
  if (clickedNode.innerHTML == "Cancel") {
    clickedNode.parentNode.parentNode.removeChild(clickedNode.parentNode);
  }
});
